print("Enter numbers: ")
numbers = [int(x) for x in input().split()]
lst = []
index = 1
for num in numbers:
    count = 0
    for num2 in numbers[index:]:
        if num2 < num:
            count += 1
    index += 1
    lst.append(count)
print("Your numbers: "+str(numbers))
print("Solution:     "+str(lst))